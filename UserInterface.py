import tkinter
from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
import tkinter.scrolledtext as tkst
import MenuCommands


root = Tk(className="pyPad")
text_pad = tkst.ScrolledText(master=root, wrap=WORD, width=20, height=10)


def open_command():
    file = filedialog.askopenfile(parent=root, mode='rb', title='Select a file...')
    if file != None:
        contents = file.read()
        text_pad.delete('1.0', END)
        text_pad.insert('1.0', contents)
        file.close()


def save_command():
    file = filedialog.asksaveasfile(parent=root, mode='w', title='Save As', defaultextension='.txt', filetypes=[('Normal text file', '*.txt'), ('All files', '*')])
    if file != None:
        # slice off the last character from get, as an extra return is added.
        data =text_pad.get('1.0', END+'-1c')
        file.write(data)
        file.close()


def exit_command():
    if messagebox.askokcancel("Quit", "Are you sure you want to quit?"):
        root.destroy()


def about_command():
    messagebox.showinfo("About", "Just another textpad... \n Copyright")

    
def test():
    print("Testing that the file menu works!")

    
def main():
    menu = Menu(root)
    root.config(menu=menu)
    file_menu = Menu(menu)
    menu.add_cascade(label="File", menu=file_menu)
    file_menu.add_command(label="New", command=test)
    file_menu.add_command(label="Open...", command=open_command)
    file_menu.add_command(label="Save", command=save_command)
    file_menu.add_separator()
    file_menu.add_command(label="Exit", command=exit_command)
    helpmenu = Menu(menu)
    menu.add_cascade(label="Help", menu=helpmenu)
    helpmenu.add_command(label="About...", command=about_command)
    # The fill option fills the entire space assigned to it.
    # The expand option assigns additional space to the widget box if the parent widget is made larger.
    text_pad.pack(padx=10, pady=10, fill=BOTH, expand=True)
    root.mainloop()
  

if __name__ == '__main__':
  main()